#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include "table.h"
#include "util.h"

struct cell {
	char str[100];
};

struct table {
	unsigned long wid;
	unsigned long hei;
	int *widest;
	struct cell *cells;
};

struct table *table_alloc(unsigned long wid, unsigned long hei)
{
	struct table *table;

	table = malloc(sizeof(struct table));
	if (table) {
		table->widest = calloc(wid, sizeof(table->widest[0]));
		table->cells = calloc(wid * hei, sizeof(struct cell));
		if (table->widest && table->cells) {
			table->wid = wid;
			table->hei = hei;
		} else {
			free(table->widest);
			free(table->cells);
			free(table);
			table = NULL;
		}
	}

	return table;
}

static struct cell *get_cell(struct table *table,
			     unsigned long x, unsigned long y)
{
	return &table->cells[(y * table->wid) + x];
}

void table_set_cell(struct table *table, unsigned long x, unsigned long y,
		    char *fmt, ...)
{
	struct cell *cell;
	va_list ap;
	int len;

	cell = get_cell(table, x, y);
	va_start(ap, fmt);
	len = vsnprintf(cell->str, sizeof(cell->str), fmt, ap);
	va_end(ap);

	table->widest[x] = max(table->widest[x], len);
}

void table_printf(struct table *table)
{
	unsigned long x;
	unsigned long y;
	struct cell *cell;

	for (y = 0; y < table->hei; y++) {
		for (x = 0; x < table->wid; x++) {
			cell = get_cell(table, x, y);
			printf("%*s ", table->widest[x], cell->str);
		}
		printf("\n");
	}
}
