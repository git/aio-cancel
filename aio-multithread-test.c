#define _GNU_SOURCE
#define _LARGEFILE_SOURCE
#define _FILE_OFFSET_BITS	64

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <linux/fs.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <libaio.h>
#include <pthread.h>

#define NR_IOCBS	256
#define NR_COMPLETIONS	1

uint64_t nr_blocks = 1024 * 1024 * 4;
int fd;
io_context_t ioctx;

uint64_t getblocks(int fd)
{
	uint64_t ret;
	struct stat statbuf;
	if (fstat(fd, &statbuf)) {
		perror("stat error\n");
		exit(EXIT_FAILURE);
	}
	ret = statbuf.st_size / 512;
	if (S_ISBLK(statbuf.st_mode))
		if (ioctl(fd, BLKGETSIZE, &ret)) {
			perror("ioctl error");
			exit(EXIT_FAILURE);
		}
	return ret / 8;
}

static void *iothread(void *p)
{
	char __attribute__((aligned(4096))) buf[4096];
	unsigned seed = 0;

	while (1) {
		struct iocb iocb[NR_IOCBS];
		struct iocb *iocbp[NR_IOCBS];
		unsigned i;
		int ret;

		memset(iocb, 0, sizeof(struct iocb) * NR_IOCBS);

		for (i = 0; i < 16; i++) {
			uint64_t offset = rand_r(&seed);

			iocb[i].aio_lio_opcode = IO_CMD_PREAD;
			iocb[i].aio_fildes = fd;

			iocb[i].u.c.buf = buf;
			iocb[i].u.c.nbytes = 4096;
			iocb[i].u.c.offset = (offset % nr_blocks) * 4096;
			//printf("offset %llu\n", iocb[i].u.c.offset);

			iocbp[i] = &iocb[i];
		}

		ret = io_submit(ioctx, NR_IOCBS, iocbp);
		if (ret < 0 && ret != -EAGAIN)
			printf("io_submit() error %i\n", ret);
		//else
		//	printf("submitted %i iocbs\n", ret);

		//io_destroy(ioctx);
		//exit(EXIT_SUCCESS);
	}

	return NULL;
}

int main(int argc, char **argv)
{
	pthread_t threads[4];
	unsigned i;

	memset(threads, 0, sizeof(pthread_t) * 4);

	if (argc != 2) {
		printf("Specify a file/device to test against\n");
		exit(EXIT_FAILURE);
	}

	fd = open(argv[1], O_RDONLY|O_DIRECT);
	if (fd < 0) {
		perror("Open error");
		exit(EXIT_FAILURE);
	}

	//nr_blocks = getblocks(fd);

	if (io_setup(INT_MAX, &ioctx) &&
	    io_setup(16384, &ioctx)) {
		perror("Error creating io context");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < 4; i++)
		if (pthread_create(&threads[i], NULL, iothread, NULL)) {
			printf("pthread_create() error\n");
			exit(EXIT_FAILURE);
		}

	while (1) {
		struct timespec timeout;
		struct io_event events[NR_COMPLETIONS];
		int ret;

		timeout.tv_sec = 0;
		timeout.tv_nsec = 10000;

		ret = io_getevents(ioctx, 1, NR_COMPLETIONS, events, &timeout);
		if (ret < 0)
			printf("io_getevents error\n");
		else {
			//printf("got %i completions\n", ret);

			for (i = 0; i < ret; i++) {
				int res = events[i].res;

				if (res < 0)
					printf("io_event err %i\n", res);
			}
		}

	}

	exit(EXIT_SUCCESS);
}
