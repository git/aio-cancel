CFLAGS := -g -O2 -Wall -Werror -D_FILE_OFFSET_BITS=64 -I.

LDLIBS := -laio

all : aio-cancel aio-multithread-test aio-thread-throughput

OBJS := $(patsubst %.c,%.o,$(wildcard *.c))
DEP_FILES := $(wildcard *.d)

ifneq ($(DEP_FILES),)
	-include $(DEP_FILES)
endif

%.o %.d: %.c Makefile
	gcc $(CFLAGS) -MD -MP -MF $*.d -c $< -o $*.o


.PHONY : clean
clean :
	-rm aio-cancel aio-multithread-test aio-thread-throughput $(OBJS) $(DEP_FILES)
