/*
 * Compile it with
 *  gcc aio.c -laio -o aio
 */

#define _GNU_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <fcntl.h>
#include <errno.h>
#include <assert.h>
#include <stdlib.h>

#include <libaio.h>

#define AIO_REQNUM 16
#define AIO_REDO_BATCH 8
#define PAGE_SIZE 4096
#define AIO_BLKSIZE	(4096 * PAGE_SIZE)

static int fd;
static long file_size;
char buffer[AIO_BLKSIZE] __attribute__ ((aligned (PAGE_SIZE)));;

static long rand_offset(void)
{
	long r = rand() % (file_size - AIO_BLKSIZE);
	long offset = r - (r % PAGE_SIZE);
	assert((offset % PAGE_SIZE) == 0);
	assert(offset >= 0);
	return offset;
}

int main(int argc, char const *argv[])
{
	io_context_t io_ctx;
	struct iocb *iop[AIO_REQNUM];
	struct iocb ioarray[AIO_REQNUM];
	struct stat stat;
	int rc;
	int i, j;
	unsigned long canceled = 0;
	unsigned long completed = 0;
	unsigned long loops;

	if (argc != 3) {
		printf("Please enter test file and number of iterations\n");
		exit(EXIT_FAILURE);
	}

	assert((long)buffer % PAGE_SIZE == 0);

	fd = open(argv[1], O_RDWR | O_DIRECT);
	if (fd < 0) {
		perror("open");
		exit(1);
	}

	loops = strtoull(argv[2], NULL, 10);
	if (!loops) {
		printf("Can't parse number of iterations\n");
		exit(EXIT_FAILURE);
	}

	fstat(fd, &stat);
	file_size = stat.st_size;
	printf("File size is %lu\n", file_size);

	memset(&io_ctx, 0, sizeof(io_ctx));
	io_queue_init(AIO_REQNUM, &io_ctx);

	for (i = 0; i < AIO_REQNUM; ++i) {
		struct iocb *iocbp = &ioarray[i];
		iop[i] = iocbp;
		io_prep_pread(iocbp, fd, buffer, AIO_BLKSIZE, rand_offset());
	}

	rc = io_submit(io_ctx, AIO_REQNUM, iop);
	if (rc < 0)
		fprintf(stderr, "io_submit: res = %d\n", rc);

	for (j = 0; j < loops; j++) {
		struct io_event events[AIO_REQNUM];
		int num = io_getevents(io_ctx, 1, AIO_REDO_BATCH, events, NULL);

		for (i = 0; i < num; i++) {
			struct io_event *event = &events[i];
			struct iocb *iocb = event->obj;

			if (event->res == -ECANCELED)
				canceled++;
			else
				completed++;

			if ((event->res != AIO_BLKSIZE &&
			     event->res != -ECANCELED) ||
			    event->res2 != 0)
				fprintf(stderr, "something wrong with event: res = %ld res2 = %ld\n", event->res, event->res2);

			iop[i] = iocb;
			io_prep_pread(iocb, fd, buffer, AIO_BLKSIZE, rand_offset());
		}
		rc = io_submit(io_ctx, num, iop);
		if (rc < 0)
			fprintf(stderr, "io_submit: res = %d\n", rc);

		for (i = 0; i < num; i++) {
			struct io_event cancelled;
			rc = io_cancel(io_ctx, iop[i], &cancelled);
			if (rc == -EINVAL)
				fprintf(stderr, "io_cancel: %d\n", rc);
		}
	}

	close(fd);

	printf("canceled %lu completed %lu\n", canceled, completed);

	return 0;
}
